﻿using _Scripts;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public GameObject Pause_image;
    public GameObject[] InPauseButtons;
    public GameObject[] InGameButtons;

    private bool isPaused = false;
    private AudioSource _musicSource;
    private AudioSource _effectSource;

    private void Awake()
    {
        _musicSource = GameObject.FindGameObjectWithTag("SM").GetComponent<SoundController>().MusicSource;
        _effectSource = GameObject.FindGameObjectWithTag("SM").GetComponent<SoundController>().EffectsSource;
        SpriteController.ResizeSpriteToScreen(Pause_image, Camera.main, 1, 1);
    }

    public bool Paused
    {
        get { return isPaused; }
    }

    public void PauseGame()
    {
        if( isPaused)
        {
            isPaused = false;
            Time.timeScale = 1;
            _musicSource.UnPause();
            _effectSource.UnPause();
            Pause_image.SetActive(false);
        }
        else
        {
            isPaused = true;
            Time.timeScale = 0;
            _musicSource.Pause();
            _effectSource.Pause();
            Pause_image.SetActive(true);
        }
        this.SwitchButtons(isPaused);
    }

    private void SwitchButtons(bool isPaused)
    {
        foreach (GameObject button in this.InPauseButtons)
        {
            button.SetActive( isPaused );
        }
        foreach (GameObject button in this.InGameButtons)
        {
            button.SetActive( ! isPaused );
        }
    }
}
