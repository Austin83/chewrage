﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace _Scripts.Bomb
{
    public class DestroyBomb : DestroyUnit
    {
        protected override void Deactivate()
        {
            this.gameObject.SetActive(false);
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if(other.gameObject.CompareTag("Player"))
            {
                SoundController.Instance.PlayEffect(SoundController.Instance._clip_boom, 0.35f);
                this.Deactivate();
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameController>().DestroyAllEnemies();
                GameObject.FindGameObjectWithTag("Player").GetComponent<Player.HealthPlayer>().Damage();
            }

            if (other.gameObject.CompareTag("Enemy"))
            {
                SoundController.Instance.PlayEffect(SoundController.Instance._clip_boom, 0.35f);
                this.Deactivate();
            }
        }

        protected override bool ToBeDeactivated(bool vertical, bool positive)
        {
            if (vertical && positive)
            {
                if (this.gameObject.transform.position.y > Constants.WorldUpperBound() + (-Constants.ItemOffset))
                {
                    return true;
                }
            }

            if (vertical && !positive)
            {
                if (this.gameObject.transform.position.y < Constants.WorldLowerBound() - (-Constants.ItemOffset))
                {
                    return true;
                }
            }

            if (!vertical && positive)
            {
                if (this.gameObject.transform.position.x > Constants.WorldRightBound() + (-Constants.ItemOffset))
                {
                    return true;
                }
            }

            if (!vertical && !positive)
            {
                if (this.gameObject.transform.position.x < Constants.WorldLeftBound() - (-Constants.ItemOffset))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
