﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Unit;

namespace _Scripts.Bomb
{
    public class MoveBomb : MoveUnit
    {
        private void Awake()
        {
            this._unitOffset = Constants.ItemOffset;
            this._spriteSize = Constants.GetSpriteSize(gameObject);
            this.RestartUnit();
            this.gameObject.SetActive(false);
        }

        public override void RestartUnit()
        {
            this.transform.position = this.RandomPosition();
            this._speed = RandomSpeed();
            this.gameObject.GetComponent<DestroyBomb>().Vertical = this._vertical;
            this.gameObject.GetComponent<DestroyBomb>().Positive = this._positive;
        }
    }
}

