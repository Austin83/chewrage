﻿using System;
using UnityEngine;

namespace _Scripts.Player
{
    public class MovePlayer : MonoBehaviour {

        [SerializeField] private float _speed;
    
        private Rigidbody2D _rb2D;
        private Animator _animator;

        private float _moveHorizontal;
        private float _moveVertical;
        private float _timeHorizontal;
        private float _timeVertical;
        private Vector2 _movement;
        private bool _still;
        private bool _knockback;
        private Vector2 _knockbackVelocity;
        private float _knockbackDuration;
        private GameObject _gameManager; 

        private void Start ()
        {
            _rb2D = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            _animator.SetBool("idle", true);
            _knockback = false;
            _knockbackDuration = Constants.KnockbackDuration;
            _gameManager = GameObject.FindGameObjectWithTag("GameManager");
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("Enemy"))
            {
                return;
            }
            
            _knockback = true;
            _knockbackDuration = Constants.KnockbackDuration;
            _knockbackVelocity = 
                (transform.position - other.transform.position).normalized * Constants.KnockbackVelocityMultiplier;  
            
        }
        
        private void FixedUpdate()
        {
            if (_knockback)
            {
                SoundController.Instance.PlayEffect(SoundController.Instance._clip_bounce, 0.25f);
                GameController.IsInputEnabled = false;
                if (Constants.KnockbackDuration >= _knockbackDuration)
                {
                    ResetAnimationConditions();
                    _animator.SetBool("stun", true);
                }
                
                _rb2D.AddForce( _knockbackVelocity, ForceMode2D.Impulse);
                _knockbackDuration -= Time.fixedDeltaTime;

                if (_knockbackDuration > 0.0f)
                {
                    return;
                }

                _rb2D.drag = Constants.PlayerFinalDrag;
                _knockbackDuration = Constants.KnockbackDuration;
                _knockback = false;
            }
            else
            {          
                _rb2D.drag = Constants.PlayerInitialDrag;
                GameController.IsInputEnabled = true;
                ResetAnimationConditions();
            }
        }

        private void Update()
        {
            if (GameController.IsInputEnabled && ! _gameManager.GetComponent<Pause>().Paused)
            {
                _moveHorizontal = Input.GetAxisRaw("Horizontal");
                _moveVertical = Input.GetAxisRaw("Vertical");

                if (Math.Abs(_moveHorizontal) >= Mathf.Epsilon)
                {
                    if (_timeHorizontal <= Mathf.Epsilon)
                        _timeHorizontal = Time.time;
                }
                else _timeHorizontal = 0.0f;

                if (Math.Abs(_moveVertical) >= Mathf.Epsilon)
                {
                    if (_timeVertical <= Mathf.Epsilon)
                        _timeVertical = Time.time;
                }
                else _timeVertical = 0.0f;

                if (_timeHorizontal >= _timeVertical)
                    _movement = new Vector2(_moveHorizontal, 0.0f);
                else if (_timeVertical > _timeHorizontal)
                    _movement = new Vector2(0.0f, _moveVertical);

                _still = Math.Abs(_moveHorizontal) <= Mathf.Epsilon && Math.Abs(_moveVertical) <= Mathf.Epsilon;


                if (_still)
                {
                    transform.Translate(Vector3.zero);
                    ResetAnimationConditions();
                    _animator.SetBool("idle", true);
                }
                else
                {
                    transform.Translate(new Vector3(_movement.x, _movement.y, 0.0f) * _speed * Time.deltaTime);
                    _animator.SetBool("idle", false);
                    Vector3 spriteSize = Constants.GetSpriteSize(gameObject);
                    float clampX = Mathf.Clamp(transform.position.x, Constants.WorldLeftBound() + spriteSize.x / 2,
                        Constants.WorldRightBound() - spriteSize.x / 2);
                    float clampY = Mathf.Clamp(transform.position.y, Constants.WorldLowerBound() + spriteSize.y / 2,
                        Constants.WorldUpperBound() - spriteSize.y / 2);
                    transform.position = new Vector3(clampX, clampY, 0.0f);
                }

                //Animation flow Control
                if (_animator.GetBool("idle") == false)
                {
                    ResetAnimationConditions();
                    if (_movement.y > 0.0f)
                    {
                        _animator.SetBool("moveUp", true);
                    }

                    if (_movement.y < 0.0f)
                    {
                        _animator.SetBool("moveDown", true);
                    }

                    if (_movement.x < 0.0f)
                    {
                        _animator.SetBool("moveLeft", true);
                    }

                    if (_movement.x > 0.0f)
                    {
                        _animator.SetBool("moveRight", true);
                    }
                }
            }
        }

        private void ResetAnimationConditions()
        {
            _animator.SetBool("idle", false);
            _animator.SetBool("moveUp", false);
            _animator.SetBool("moveDown", false);
            _animator.SetBool("moveRight", false);
            _animator.SetBool("moveLeft", false);
            _animator.SetBool("stun", false);
        }
    }
}
