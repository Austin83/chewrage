﻿using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


namespace _Scripts.Player
{
	public class ScorePlayer : MonoBehaviour {
        
		public TextMeshProUGUI ScoreText;
		
		private int _score;
		
		public int Score
		{
			
			get { return _score; }
			
			set
			{
				_score = value;
				this.UpdateScoreText();
			}
		}

		private void Awake()
		{
			this._score = 0;
			this.UpdateScoreText();
		}
		
		private void UpdateScoreText()
		{
			this.ScoreText.text = "Score: " + this._score.ToString();
		}
	}
}
