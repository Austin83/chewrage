﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Scripts.Player
{
    public class HealthPlayer : MonoBehaviour {
		
        private int _health;
        private GameObject _healthbar;

        public int Health
        {
            get { return _health; }
        }
        
        private void Awake()
        {
            this._health = Constants.LivesMax;
            _healthbar = GameObject.FindWithTag("Health");
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if( other.gameObject.CompareTag("Enemy") )
            {
                Damage();
            }
            
        }

        public void Damage()
        {
            if (_health > 0)
            {
                --_health;
            }
            _healthbar.GetComponent<Healthbar>().Damage();
            if (_health == 0)
            {
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameController>().GameOver = true;
                this.gameObject.SetActive(false);
            }
        }
    }
}

