﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Analytics;
using Random = UnityEngine.Random;
using TMPro;
using _Scripts.Player;
using _Scripts.Bomb;
using _Scripts.Enemy;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace _Scripts
{
	public class GameController : MonoBehaviour
	{
        public TextMeshProUGUI ScoreText;
        public TextMeshProUGUI FinalScoreText;
        public TextMeshProUGUI HighScoreText;
        public TextMeshProUGUI InGameHighScoreText;
        public TextMeshProUGUI Pause_prompt;
        public Image Healthbar;
        public GameObject GameOverMenu;

        public static bool IsInputEnabled = true;
		[SerializeField] private GameObject _enemyPrefab;
		[SerializeField] private List<GameObject> _foodPrefabs = new List<GameObject>();
        [SerializeField] private GameObject _bombPrefab;

        private static GameController _instance = null;

		private readonly List<GameObject> _foodPool = new List<GameObject>();
		private readonly List<GameObject> _enemyPool = new List<GameObject>();

		private GameObject _player;
		private GameObject _bomb;
        private bool _gameOver;
        private int _dificultyLimit;

        public bool GameOver
        {
            get { return _gameOver;  }
            set { _gameOver = value; }
        }

		private void Awake()
		{
			this._player = GameObject.FindGameObjectWithTag("Player");
            this._gameOver = false;
			this.LazyLoad();
			this.LoadFoodPool();
			this.LoadEnemy();
            this._dificultyLimit = Random.Range(1, Constants.LimitMax);
            this._bomb = Instantiate(_bombPrefab);
            Time.timeScale = 1;
        }

		void Start () {
            InGameHighScoreText.text = "Record: " + GetComponent<DataController>().HighScore.ToString();
            SoundController.Instance.PlayMusic(SoundController.Instance._clip_main_loop, 0.60f);
        }
	
		// Update is called once per frame
		void Update ()
		{
            if( !_gameOver)
            {
                if( Input.GetKeyDown(KeyCode.Space) )
                {
                    gameObject.GetComponent<Pause>().PauseGame();
                }
                AdjustDificulty();
                GetBomb();
            }
            if( _gameOver )
            {
                EndGame();
            }
		}

        private void OnEnable()
        {
            _gameOver = false;
        }

        private void LazyLoad()
		{
			if (_instance == null)
				_instance = this;
			else if(_instance != this )
				Destroy(this.gameObject);
		}

        public void DestroyAllEnemies()
        {
            foreach ( GameObject enemy in _enemyPool )
            {
                enemy.SetActive(false);
            }
        }

        private void LoadFoodPool()
		{
			int max = 0;

			for ( int index = 0 ; index < _foodPrefabs.Count ; index++)
			{
				switch (index)
				{
					case 0:
						max = Constants.CandyFoodPool;
						break;
					case 1:
						max = Constants.ChocolateFoodPool;
						break;
					case 2:
						max = Constants.PizzaFoodPool;
						break;
						
				}
				this.LoadFood(index, max);	
			}
		}

		private void LoadFood(int foodType, int max)
		{
			
			for (  int i = 0; i < max; i++)
			{
				GameObject item = Instantiate(_foodPrefabs[foodType]);
				
				item.transform.SetParent(this.gameObject.transform);
				this._foodPool.Add(item);
			}
		}

		private void AdjustDificulty()
		{
            int score = _player.GetComponent<Player.ScorePlayer>().Score;
			if ( score > this._dificultyLimit )
			{
                if( _foodPool.Count > Constants.FoodMinimum)
                {
                    DecreaseFoodPool();
                }
                IncreaseFoodSpeed();
				LoadEnemy();
				IncreaseEnemiesSpeed();
                this._dificultyLimit += Random.Range(1, Constants.LimitMax);
            }
        }

        private void LoadEnemy()
        {
            int max = Random.Range(0, Constants.DificultyFactor - 1);
            for (int i = 0; i <= max; i++)
            {
                GameObject enemy = GetInactiveEnemy();
                if( enemy != null )
                {
                    enemy.SetActive(true);
                    _enemyPool.Add(enemy);
                    enemy.GetComponent<MoveEnemy>().RestartUnit();
                    continue;
                }
                enemy = Instantiate(_enemyPrefab);
                if (_enemyPool.Count > 0)
                {
                    int index = Random.Range(0, _enemyPool.Count - 1);
                    enemy.GetComponent<Enemy.MoveEnemy>().SpeedAverage = 
                        _enemyPool[index].GetComponent<Enemy.MoveEnemy>().SpeedAverage;
                }
                enemy.transform.SetParent(this.gameObject.transform);
                _enemyPool.Add(enemy);
            }
        }

        private void GetBomb()
        {
            if( _player.GetComponent<HealthPlayer>().Health * 1.5 <= GetActiveEnemies() && ! _bomb.activeSelf )
            {
                _bomb.GetComponent<MoveBomb>().RestartUnit();
                _bomb.SetActive(true);
            }
        }

        private int GetActiveEnemies()
        {
            int result = 0;
            foreach( GameObject enemy in _enemyPool)
            {
                if( enemy.activeSelf )
                {
                    result++;
                }
            }
            return result;
        }

        private GameObject GetInactiveEnemy()
        {
            foreach( GameObject enemy in _enemyPool)
            {
                if ( !enemy.activeSelf )
                {
                    return enemy;
                }
            }
            return null;
        }

        private void IncreaseFoodSpeed()
		{
			foreach (GameObject item in _foodPool)
			{
				item.GetComponent<Food.MoveFood>().SpeedAverage += 
                    Constants.SpeedAcceleration * Random.Range(1, Constants.DificultyFactor);
			}		
		}
		
		private void IncreaseEnemiesSpeed()
		{
            foreach (GameObject enemy in _enemyPool)
			{
				enemy.GetComponent<Enemy.MoveEnemy>().SpeedAverage += 
                    Constants.SpeedAcceleration * Random.Range(1,Constants.DificultyFactor);
			}		
		}

		private void DecreaseFoodPool()
		{
            int max = Random.Range(0, Constants.DificultyFactor - 1);
            for ( int i = 0; i <= max; i++ )
            {
                int index = Random.Range(0, _foodPool.Count - 1);
                _foodPool[index].SetActive(false);
                GameObject someFood = _foodPool[index];
                _foodPool.RemoveAt(index);
                Destroy(someFood);
            }
            //Debug.Log("Food left: " + _foodPool.Count + " pieces");
		}

        private void EndGame()
        {
            SoundController.Instance.MusicSource.Stop();
            SoundController.Instance.PlayEffect(SoundController.Instance._clip_game_over, 0.5f);
            EnableGameOverMenu();
            DisableGameUI();
            this.gameObject.SetActive(false);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void BackToMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        private void DisableGameUI()
        {
            this.Healthbar.enabled = false;
            this.ScoreText.text = "";
            foreach(GameObject button in this.GetComponent<Pause>().InGameButtons)
            {
                button.SetActive(false);
            }
            InGameHighScoreText.text = "";
            _player.SetActive(false);
            _bomb.SetActive(false);
        }

        private void EnableGameOverMenu()
        {
            FinalScoreText.text = ScoreText.text + " points";
            GetComponent<DataController>().SubmitNewPlayerScore(_player.GetComponent<ScorePlayer>().Score);
            HighScoreText.text = "High Score: " + GetComponent<DataController>().HighScore + " points";
            GameOverMenu.SetActive(true);
            GameOverMenu.GetComponent<GameOverMenu>().enabled = true;
        }
 
	}
}
