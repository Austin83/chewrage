﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DestroyUnit : MonoBehaviour {

	protected bool _vertical;
	protected bool _positive;
        
	public bool Vertical
	{
		set { _vertical = value; }
	}
        
	public bool Positive
	{
		set { _positive = value; }
	}

	private void Update()
	{
		if ( this.ToBeDeactivated(this._vertical, this._positive) )
		{
			this.Deactivate();
		}
	}

	protected abstract void Deactivate();
	protected abstract bool ToBeDeactivated(bool vertical, bool positive);
	protected abstract void OnTriggerEnter2D(Collider2D other);
}
