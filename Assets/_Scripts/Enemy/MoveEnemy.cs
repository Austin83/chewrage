﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Unit;

namespace _Scripts.Enemy
{
    public class MoveEnemy : MoveUnit
    {
        private static int _orderInLayer = 1;
        private Animator _animator;

        private void SetAnimation()
        {
            if (this._vertical && this._positive)
            {
                _animator.SetInteger("Direction", Constants.EnemyUp);
            }
            
            if ( !this._vertical && this._positive)
            {
                _animator.SetInteger("Direction", Constants.EnemyRight);
            }
            
            if (this._vertical && !this._positive)
            {
                _animator.SetInteger("Direction", Constants.EnemyDown);
            }
            
            if ( !this._vertical && !this._positive)
            {
                _animator.SetInteger("Direction", Constants.EnemyLeft);
            }
        }
        
        private void Awake()
        {
            this._animator = GetComponent<Animator>();
            this._unitOffset = Constants.EnemyOffset;
            this._spriteSize = Constants.GetSpriteSize(gameObject);
            gameObject.GetComponent<SpriteRenderer>().sortingOrder = _orderInLayer++;
            this.RestartUnit();
        }

        public override void RestartUnit()
        {
            this.gameObject.SetActive(true);
            this.transform.position = this.RandomPosition();
            this._speed = RandomSpeed();
            this.gameObject.GetComponent<DestroyEnemy>().Vertical = this._vertical;
            this.gameObject.GetComponent<DestroyEnemy>().Positive = this._positive;
            SetAnimation();
        }
        
    }
}