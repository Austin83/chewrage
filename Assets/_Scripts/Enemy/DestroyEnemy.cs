﻿using System;
using UnityEngine;

namespace _Scripts.Enemy
{
	public class DestroyEnemy : DestroyUnit {
	
		protected override void OnTriggerEnter2D(Collider2D other)
		{
            return;
		}
        
		protected override void Deactivate()
		{
			gameObject.SetActive(false);
			gameObject.GetComponent<MoveEnemy>().RestartUnit();
		}

		protected override bool ToBeDeactivated(bool vertical, bool positive){
            
			if( vertical && positive )
			{
				if (this.gameObject.transform.position.y > Constants.WorldUpperBound() + (-Constants.EnemyOffset) )
				{
					return true;
				}
			}

			if ( vertical && !positive )
			{
				if (this.gameObject.transform.position.y < Constants.WorldLowerBound() - (-Constants.EnemyOffset) )
				{
					return true;
				}
			}

			if (!vertical && positive)
			{
				if (this.gameObject.transform.position.x > Constants.WorldRightBound() + (-Constants.ItemOffset) )
				{
					return true;
				}
			}

			if (!vertical && !positive)
			{
				if (this.gameObject.transform.position.x < Constants.WorldLeftBound() - (-Constants.ItemOffset) )
				{
					return true;
				}
			}
        
			return false;
		}
	
	}
}
