﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
    public class Constants : MonoBehaviour
    {
        public const float ItemOffset = -0.75f;
        public const float EnemyOffset = -1.0f;
        public const int CandyFoodPool = 25;
        public const int ChocolateFoodPool = 18;
        public const int PizzaFoodPool = 8;
        public const int FoodMaximumPoolStrip = 85;
        public const int ScorePizza = 10;
        public const int ScoreChocolate = 5;
        public const int ScoreCandy = 2;
        public const int FoodDificultyLimit = 1000;
        public const int FoodMinimum = 15;
        
        public const float SpeedMultiplierMinimum = 0.5f;
        public const float SpeedMultiplierMaximum = 1.5f;
        public const float SpeedAcceleration = 0.1f;
        
        public const float KnockbackDuration = 0.15f;
        public const int KnockbackVelocityMultiplier = 2;

        public const int PlayerInitialDrag = 5;
        public const int PlayerFinalDrag = 50;

        public const int EnemyUp = 1; 
        public const int EnemyRight = 2; 
        public const int EnemyDown = 3; 
        public const int EnemyLeft = 4;

        public const int LivesMax = 10;
        public const int LimitMax = 500;
        public const int DificultyFactor = 5;

        public const string PausePrompt = "Pause: press P";
        public const string UnpausePrompt = "Continue: press P";
        public const string QuitPrompt = "Quit game? Hold Q";
        public const string MainMenuPrompt = "Back to menu? Hold B";

        public const float HoldDuration = 2f;

        public static float WorldLowerBound()
        {
            return -WorldUpperBound();
        }

        public static float WorldUpperBound()
        {
            return Camera.main.orthographicSize;
        }
        
        public static float WorldLeftBound()
        {
            return -WorldRightBound();
        }
        
        public static float WorldRightBound()
        {
            return Camera.main.orthographicSize * Screen.width / Screen.height;
        }

        public static Vector3 GetSpriteSize(GameObject var)
        {
            return var.GetComponent<SpriteRenderer>().bounds.size;
        }
    }
}