﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataController : MonoBehaviour
{
    private PlayerProgress playerProgress;
    private string gameDataFileName = "data.json";

    public int HighScore
    {
        get
        {
            return playerProgress.highScore;
        }

        set
        {
            playerProgress.highScore = value;
        }
    }

    void Awake()
    {
        LoadPlayerProgress();
    }

    public void SubmitNewPlayerScore(int newScore)
    {
        if ( newScore > HighScore )
        {
            HighScore = newScore;
            SavePlayerProgress();
        }
    }

    private void LoadPlayerProgress()
    {
        string filePath = Path.Combine(Application.dataPath, gameDataFileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            playerProgress = JsonUtility.FromJson<PlayerProgress>(dataAsJson);
        }
        else
        {
            Debug.LogError("Cannot load game data!");
        }
    }

    private void SavePlayerProgress()
    {
        string dataAsJson = JsonUtility.ToJson(playerProgress);
        string filePath = Path.Combine(Application.dataPath, gameDataFileName);
        File.WriteAllText(filePath, dataAsJson);
    }

}
