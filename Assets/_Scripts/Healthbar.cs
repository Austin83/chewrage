﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public List<Sprite> sprites;

    private Image _imageUI; 
    private int _index; 

    public void Damage()
    {
        if( _index < sprites.Count())
        {
            _imageUI.sprite = sprites[_index++];
        }
    }

    private void Awake()
    {
        _imageUI = gameObject.GetComponent<Image>();
        _index = 0;
    }
}
